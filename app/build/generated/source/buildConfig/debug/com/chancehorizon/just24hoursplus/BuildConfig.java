/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.chancehorizon.just24hoursplus;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.chancehorizon.just24hoursplus";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 10401;
  public static final String VERSION_NAME = "1.4.1";
}
