package com.chancehorizon.just24hoursplus

import android.app.Activity
import android.os.Bundle
import android.content.Intent
import android.content.pm.PackageInfo
import android.widget.TextClock
import android.view.GestureDetector
import android.view.View.OnTouchListener
import android.view.MotionEvent
import android.content.pm.PackageManager
import android.view.animation.AnimationUtils
import android.view.GestureDetector.SimpleOnGestureListener
import android.graphics.drawable.ColorDrawable
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.Point
import android.os.Build
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import java.lang.Exception
import android.widget.Toast
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat


class theClockActivity : Activity() {

    var invert = false

    var noSleep = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val windowInsetsController =
                    ViewCompat.getWindowInsetsController(window.decorView) ?: return
            // Configure the behavior of the hidden system bars
            windowInsetsController.systemBarsBehavior =
                    WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            // Hide both the status bar and the navigation bar
            windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())
        }
        else {
            val decorView = window.decorView
            val uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            decorView.systemUiVisibility = uiOptions
        }

        val welcomeIntent = Intent(this, WelcomeActivity::class.java)
        setContentView(R.layout.activity_the_clock)
        val clockBrightness: Int
        val pInfo: PackageInfo // info stored in the app package - used to retrieve the app version number

        // retrieve save user settings
        val settings = getSharedPreferences("UserInfo", 0)
        // retrieve the user set clock brightness
        clockBrightness = settings.getInt("clockBrightness", 255)
        // retrieve the user set invert (bright text or bright background)
        invert = settings.getBoolean("invert", false)
        // retrive the user setting for keeping the screen always on
        noSleep = settings.getBoolean("noSleep", false)

        setScreenTimeout()

        // set the brightness of the clock to the save user set brightness
        val theClockView = findViewById<TextClock>(R.id.textClock)

        // set if the text or the background is bright (rather than pure black)
        if (invert) {
            // background is set to default or user set brightness
            theClockView.setBackgroundColor(Color.rgb(clockBrightness, clockBrightness, clockBrightness))
            theClockView.setTextColor(Color.rgb(0, 0, 0))
        } else {
            // text is set to default or user set brightness
            theClockView.setBackgroundColor(Color.rgb(0, 0, 0))
            theClockView.setTextColor(Color.rgb(clockBrightness, clockBrightness, clockBrightness))
        }

        // set up the clock view to detect gestures (swipes and flings).
        val gestureDetector = GestureDetector(this, SwipeGestureDetector())
        val gestureListener = OnTouchListener { v, event -> gestureDetector.onTouchEvent(event) }

        theClockView.setOnTouchListener(gestureListener)

        // determine if this is the first time running this version of the application
        try {
            pInfo = packageManager.getPackageInfo(packageName, 0)
            val lastVersionCode = settings.getInt("AppVersion", -1)
            val currentVersionCode = pInfo.versionCode
            if (lastVersionCode < 10400) {
                startActivity(welcomeIntent)
            }
            // Update version in preferences - will be used to detect first run in future versions
            settings.edit().putInt("AppVersion", currentVersionCode).apply()
        } catch (e: PackageManager.NameNotFoundException) {
            // nothing
        }
    }



    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        // resize the clock for orientation changes
        setClockSize()
    }



    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // set the clock to display as large as possible without extending beyond the screen extents
        val theClockView = findViewById<TextClock>(R.id.textClock)
        // retrieve the animation details from the anim resource file (XML)
        val startAnimation = AnimationUtils.loadAnimation(applicationContext, R.anim.startanimation)
        startAnimation.reset()
        theClockView.clearAnimation()
        theClockView.startAnimation(startAnimation)
    }



    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)

        // resize the clock (to handle rotation of the screen)
        setClockSize()
    }



    // make sure that the clock fills as much of the screen as possible (when starting the application and when
    //  rotating the screen
    private fun setClockSize() {
        // set the clock to display as large as possible without extending beyond the screen extents
        val theClockView = findViewById<TextClock>(R.id.textClock)
        theClockView.refreshDrawableState()
        var clockTextSize = 10
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val maxClockWidth = size.x

        // start with a small text size
        var textWidth = 10
        while (textWidth < maxClockWidth - 10 && theClockView.text !== "") {
            clockTextSize = clockTextSize + 1
            theClockView.textSize = clockTextSize.toFloat()
            theClockView.measure(0, 0) // check the size of the clock!
            textWidth = theClockView.measuredWidth //get the clock's width//
        }
    }



    override fun onBackPressed() {
        finish()
    }



    // react to user swipes on the clock screen
    internal inner class SwipeGestureDetector : SimpleOnGestureListener() {

        // toggle keeping the screen on when the user long presses anywhere on the screen
        override fun onLongPress(e: MotionEvent?) {
            super.onLongPress(e)

            noSleep = !noSleep

            setScreenTimeout()

            // save the colour settings so that they appear the next runtime
            val settings = getSharedPreferences("UserInfo", 0)
            val editor = settings.edit()
            editor.putBoolean("noSleep", noSleep)
            editor.apply()

        }



        // flinging from left to right quits the application
        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            try {
                // retrieve the start and end point of the fling
                val deltaX = e2.x - e1.x
                // float deltaY = e2.getY() - e1.getY(); *** not used yet

                // right to left fling to show first start tutorial
                if (deltaX < SWIPE_MIN_DISTANCE * -1 && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    // show the first start tutorial
                    val welcomeIntent = Intent(this@theClockActivity, WelcomeActivity::class.java)
                    startActivity(welcomeIntent)
                }

                // left to right fling to quit the application
                if (deltaX > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    val theClockView = findViewById<TextClock>(R.id.textClock)

                    // retrieve the animation details from the anim resource file (XML)
                    val quitAnimation = AnimationUtils.loadAnimation(applicationContext, R.anim.quitanimation)
                    quitAnimation.reset()
                    theClockView.clearAnimation()
                    theClockView.startAnimation(quitAnimation)

                    // set the delay to quit application to the animiation time
                    val theDelay = quitAnimation.duration.toInt()

                    // delay quitting the clock application so that the animation is viewable
                    theClockView.postDelayed({ // quit the clock application
                        finish()
                        System.exit(0)
                    }, theDelay.toLong())
                }
            } catch (e: Exception) {
                // nothing
            }
            return false
        }



        // allow user to swipe up and down to set the "brightness" (the range of black to grey to white) of the clock text or clock background (whichever is not black)
        override fun onScroll(e1: MotionEvent, e2: MotionEvent, deltaX: Float, deltaY: Float): Boolean {
            val theClockView = findViewById<TextClock>(R.id.textClock)
            val screenOnIcon = findViewById<ImageView>(R.id.screenOnIcon)
            val display = windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)

            // determine the percentage of the screen that the user has swiped (used to determine the amount of brightness to change)
            // float xIncrease =  Math.abs(deltaX)/size.x;  ***not used yet
            val yIncrease = Math.abs(deltaY) / size.y
            var color = theClockView.currentTextColor
            var newColor = color
            if (invert) {
                // extract the clock's background colour
                val background = theClockView.background
                if (background is ColorDrawable) {
                    color = background.color
                }
            }

            // make sure that the swipe is mostly a vertical one
            if (Math.abs(deltaY) > 10 && deltaX < 30) {
                // swipe down to decrease clock brightness
                if (-deltaY > SCROLL_MIN_DISTANCE) {
                    newColor = Math.max(Color.red(color) - (yIncrease * 255).toInt(), 0)
                } else if (deltaY > SCROLL_MIN_DISTANCE) {
                    newColor = Math.min(Color.red(color) + (yIncrease * 255).toInt(), 255)
                }

                // set the new brightness of the clock
                if (invert) {
                    theClockView.setBackgroundColor(Color.rgb(newColor, newColor, newColor))
                    screenOnIcon.setBackgroundColor(Color.rgb(newColor, newColor, newColor))
                } else {
                    theClockView.setTextColor(Color.rgb(newColor, newColor, newColor))
                    screenOnIcon.setColorFilter(Color.rgb(newColor, newColor, newColor))
                }

                // save the colour settings so that they appear the next runtime
                val settings = getSharedPreferences("UserInfo", 0)
                val editor = settings.edit()
                editor.putInt("clockBrightness", newColor)
                editor.putBoolean("invert", invert)
                editor.apply()
            }
            return true
        }



        override fun onDown(e: MotionEvent): Boolean {
            return true
        }



        // invert clock and background brightness
        override fun onDoubleTap(e: MotionEvent): Boolean {
            // get the clock object
            val theClockView = findViewById<TextClock>(R.id.textClock)
            val screenOnIcon = findViewById<ImageView>(R.id.screenOnIcon)
            // retrieve the colours of the clock text and clock background
            val color = theClockView.currentTextColor
            var bgColor = Color.rgb(0, 0, 0) // default background colour to black

            // extract the clock's background colour
            val background = theClockView.background
            if (background is ColorDrawable) {
                bgColor = background.color
            }

            // swap the clock's text and background colours
            theClockView.setTextColor(Color.rgb(bgColor, bgColor, bgColor))
            theClockView.setBackgroundColor(Color.rgb(color, color, color))
            screenOnIcon.setColorFilter(Color.rgb(bgColor, bgColor, bgColor))
            screenOnIcon.setBackgroundColor(Color.rgb(color, color, color))

            // save the colour settings so that they appear the next runtime
            val settings = getSharedPreferences("UserInfo", 0)
            val editor = settings.edit()

            // change the invert flag (true becomes false, false becomes true
            invert = !invert
            editor.putBoolean("invert", invert)
            editor.apply()
            return true
        }
    }



    fun setScreenTimeout() {

        val screenOnIcon = findViewById<ImageView>(R.id.screenOnIcon)

        // let the screen timeout (turn off after no user interaction)
        if (noSleep) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            Toast.makeText(getApplicationContext(),"Screen Will Timeout",Toast.LENGTH_SHORT).show()
            screenOnIcon.visibility = View.INVISIBLE
        }
        // prevent the screen from timing out (keep the screen on forever - or at least until the app is dismissed)
        else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            Toast.makeText(getApplicationContext(),"Screen Will Stay On",Toast.LENGTH_SHORT).show()
            screenOnIcon.visibility = View.VISIBLE
        }

    }



    companion object {
        private const val SWIPE_MIN_DISTANCE = 400 // minimum distance to consider gesture a swipe
        private const val SCROLL_MIN_DISTANCE = 1 // minimum distance to consider gesture a scroll
        private const val SWIPE_THRESHOLD_VELOCITY = 200 // minimum velocity to consider gesture a swipe
    }
}