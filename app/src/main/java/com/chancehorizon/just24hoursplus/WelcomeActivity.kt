package com.chancehorizon.just24hoursplus

import android.app.Activity
import android.graphics.Color
import androidx.viewpager.widget.ViewPager
import android.widget.LinearLayout
import android.os.Bundle
import android.os.Build
import android.widget.TextView
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import android.view.animation.AnimationUtils
import android.view.WindowManager
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat


class WelcomeActivity : Activity() {
    private var viewPager: ViewPager? = null
    private var dotsLayout: LinearLayout? = null
    private lateinit var layouts: IntArray
    private lateinit var animations: IntArray
    private lateinit var animations2: IntArray
    private lateinit var animClocks: IntArray
    private lateinit var animGestures: IntArray
    private var btnSkip: Button? = null
    private var btnNext: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Making notification bar transparent
        WindowCompat.setDecorFitsSystemWindows(window, false)

//        if (Build.VERSION.SDK_INT >= 21) {
//            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val windowInsetsController =
                    ViewCompat.getWindowInsetsController(window.decorView) ?: return
            // Configure the behavior of the hidden system bars
            windowInsetsController.systemBarsBehavior =
                    WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            // Hide both the status bar and the navigation bar
            windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())
        }
        else {
            //***window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
            val decorView = window.decorView
            val uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            decorView.systemUiVisibility = uiOptions
        }

        setContentView(R.layout.activity_welcome)
        viewPager = findViewById<View>(R.id.view_pager) as ViewPager
        dotsLayout = findViewById<View>(R.id.layoutDots) as LinearLayout
        btnSkip = findViewById<View>(R.id.btn_skip) as Button
        btnNext = findViewById<View>(R.id.btn_next) as Button


        // layouts of all welcome pages
        layouts = intArrayOf(
                R.layout.welcome1,
                R.layout.welcome2,
                R.layout.welcome3,
                R.layout.welcome4,
                R.layout.welcome5,
                R.layout.welcome6,
                R.layout.welcome7
        )

        // animations for all welcome pages
        animations = intArrayOf(
                R.anim.welcomeanimation1,
                R.anim.welcomeanimation2,
                R.anim.welcomeanimation3,
                R.anim.welcomeanimation4,
                0,
                R.anim.welcomeanimation6,
                0
        )

        // animations for all welcome pages
        animations2 = intArrayOf(
                0,
                R.anim.welcomeanimation22,
                R.anim.welcomeanimation23,
                R.anim.welcomeanimation24,
                0,
                R.anim.welcomeanimation26,
                0
        )

        // clockviews on each page to be animated
        animClocks = intArrayOf(
                R.id.textClock1,
                R.id.textClock2,
                R.id.textClock3,
                R.id.textClock4,
                0,
                R.id.textClock6,
                0
        )

        // clockviews on each page to be animated
        animGestures = intArrayOf(
                0,
                R.id.imageGesture2,
                R.id.imageGesture3,
                R.id.imageGesture4,
                0,
                R.id.imageGesture6,
                0
        )

        // adding bottom dots
        addBottomDots(0)

        // making notification bar transparent
        changeStatusBarColor()
        val myViewPagerAdapter = MyViewPagerAdapter()
        viewPager!!.adapter = myViewPagerAdapter
        viewPager!!.addOnPageChangeListener(viewPagerPageChangeListener)

        // close the welcome/help screen and go back to the clock dispay
        btnSkip!!.setOnClickListener { displayClockScreen() }

        // display the next page in the welcome/help
        btnNext!!.setOnClickListener {
            // checking for last page
            // if last page clock screen will be displayed
            val current = getItem(+1)
            if (current < layouts.size) {
                // move to next screen
                viewPager!!.currentItem = current
            } else {
                // close the welcome/help screen and display the clock
                displayClockScreen()
            }
        }
    }

    // display the dots on the bottom of the screen that show progress through the pages
    private fun addBottomDots(currentPage: Int) {
        val dots = arrayOfNulls<TextView>(layouts.size)
        val colorsActive = resources.getIntArray(R.array.array_dot_active)
        val colorsInactive = resources.getIntArray(R.array.array_dot_inactive)
        dotsLayout!!.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(this)
            dots[i]!!.text = "\u2022"
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(colorsInactive[currentPage])
            dotsLayout!!.addView(dots[i])
        }
        if (dots.size > 0) {
            dots[currentPage]!!.setTextColor(colorsActive[currentPage])
        }
    }



    private fun getItem(i: Int): Int {
        return viewPager!!.currentItem + i
    }



    // end the welcome/help screens and display the clock
    private fun displayClockScreen() {
        finish()
    }



    //	viewpager change listener
    var viewPagerPageChangeListener: OnPageChangeListener = object : OnPageChangeListener {
        override fun onPageSelected(pageNumber: Int) {
            addBottomDots(pageNumber)

            // changing the next button text 'NEXT' / 'GOT IT'
            if (pageNumber == layouts.size - 1) {
                // last page. make button text to GOT IT
                btnNext!!.text = getString(R.string.start)
                btnSkip!!.visibility = View.GONE

                // insert the version number
                val versionname = findViewById<View>(R.id.NameAndVersion) as TextView
                versionname.text = getString(R.string.slide_7_title, BuildConfig.VERSION_NAME)
            } else {
                // still pages are left
                btnNext!!.text = getString(R.string.next)
                btnSkip!!.visibility = View.VISIBLE
            }

            // only create and play a clock animation if the page has one and also has a clock image to animate
            if (animations[pageNumber] != 0 && animClocks[pageNumber] != 0) {
                // each welcome/help page has an animation - select the textclock element to be animated
                val theClockView = findViewById<View>(animClocks[pageNumber]) as ImageView
                theClockView.visibility = View.VISIBLE

                // retrieve the animation details from the anim resource file (XML) for this welcome page
                val welcomeAnimation = AnimationUtils.loadAnimation(applicationContext, animations[pageNumber])
                welcomeAnimation.reset()
                theClockView.clearAnimation()
                theClockView.startAnimation(welcomeAnimation)
            }

            // only create and play a gesture animation if the page has one and also has a gesture image to animate
            if (animations2[pageNumber] != 0 && animGestures[pageNumber] != 0) {
                // each welcome/help page has an animation - select the textclock element to be animated
                val theGestureView = findViewById<View>(animGestures[pageNumber]) as ImageView
                theGestureView.visibility = View.VISIBLE

                // retrieve the animation details from the anim resource file (XML) for this welcome page
                val welcomeAnimation2 = AnimationUtils.loadAnimation(applicationContext, animations2[pageNumber])
                welcomeAnimation2.reset()
                theGestureView.clearAnimation()
                theGestureView.startAnimation(welcomeAnimation2)
            }
        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}
        override fun onPageScrollStateChanged(arg0: Int) {}
    }



    // Making notification bar transparent
    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
        }
    }



    // View pager adapter
    inner class MyViewPagerAdapter : PagerAdapter() {
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val layoutInflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = layoutInflater.inflate(layouts[position], container, false)
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return layouts.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }
}